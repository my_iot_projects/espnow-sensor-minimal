#include <Arduino.h>

/*
 ESP-NOW based sensor 

 Sends readings to an ESP-Now server with a fixed mac address

 Anthony Elder
 License: Apache License v2
*/
extern "C" {
  #include <espnow.h>
}

// this is the MAC Address of the remote ESP server which receives these sensor readings
uint8_t remoteMac[] = {0x8c, 0xaa, 0xb5, 0x1b, 0x7b, 0x89};

#define WIFI_CHANNEL 1
//#define SLEEP_SECS 15 * 60 // 15 minutes
#define SLEEP_SECS 5  // 15 minutes
#define SEND_TIMEOUT 100  // 245 millis seconds timeout 

//Test
static const uint8_t ESPNOW_MAGIC = 0xA5;
uint16_t _num;

enum espnow_type_t : uint8_t { ESPNOW_ACK, ESPNOW_DATA };

struct __packed espnow_header_t {
  uint8_t magic; // Must be == ESPNOW_MAGIC (0xA5)
  espnow_type_t type;
  uint16_t num;
};

struct __packed payload_t {
  uint32_t id;
  uint32_t uptime;
};

struct __packed espnow_data_t {
  espnow_header_t header;
  payload_t payload;
};
//Test

// keep in sync with slave struct
struct __attribute__((packed)) SENSOR_DATA {
  float temp;
} sensorData;

void gotoSleep() {
  // add some randomness to avoid collisions with multiple devices
  int sleepSecs = SLEEP_SECS + ((uint8_t)RANDOM_REG32/8); 
  Serial.printf("Awake for %i ms, going to sleep for %i secs...\n", millis(), sleepSecs); 
  ESP.deepSleepInstant(sleepSecs * 1000000, RF_NO_CAL);
}

void setup() {
  Serial.begin(115200); Serial.println();

  if (esp_now_init() != 0) {
    Serial.println("*** ESP_Now init failed");
    gotoSleep();
  }

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_add_peer(remoteMac, ESP_NOW_ROLE_SLAVE, WIFI_CHANNEL, NULL, 0);

  esp_now_register_send_cb([](uint8_t* mac, uint8_t sendStatus) {
    Serial.printf("send_cb, send done, status = %i\n", sendStatus);
    gotoSleep();
  });

  //uint8_t bs[sizeof(sensorData)];
  //memcpy(bs, &sensorData, sizeof(sensorData));
  //esp_now_send(NULL, bs, sizeof(sensorData)); // NULL means send to all peers
  
  espnow_data_t data;
  data.header.magic = ESPNOW_MAGIC;
  data.header.type = ESPNOW_DATA;
  data.header.num = ++_num;
  data.payload.id = ESP.getChipId();
  data.payload.uptime = millis();
  esp_now_send(NULL, (uint8_t*)&data, sizeof(data));
  //esp_now_send((uint8_t*)mac, (uint8_t*)&data, sizeof(data))
}

void loop() {
  if (millis() > SEND_TIMEOUT) {
    gotoSleep();
  }
}

